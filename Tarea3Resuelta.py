# Ejercicio 1
"""
numeros_ganadores = []
while True:
	comparar = input('Ingrese los numeros ganadores\nSi desea salir, presione q.\n')
	if comparar == 'q':
		break
	else:
		numeros = int(comparar)
		numeros_ganadores.append(numeros)
		continue
numeros_ganadores.sort()
print(numeros_ganadores)
"""	

# Ejercicio 2
"""
lista_numeros = [1,2,3,4,5,6,7,8,9,10]
lista_numeros.reverse()

for i in lista_numeros:
	print(i,end=',')
"""

# Ejercicio 3
"""
asignaturas = ['Matemáticas','Física','Química','Historia','Lengua']
comparar = []
notas = []

for x in asignaturas:
	nota = int(input('Cuanto sacaste en {}: '.format(x)))
	if nota <= 4:
		comparar.append(x)
		notas.append(nota)

for j in comparar:
	asignaturas.remove(j)
	print('Debes repertir ' + j + ' porque sacaste mala nota.')
"""
# Ejercicio 4
"""
abecedario = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z']


for i in range(len(abecedario),1,-1):
	if i % 3 == 0:
		abecedario.pop(i-1)
print(abecedario)
"""

# Ejercicio 5
"""
palabra = input('Ingrese una palabra: ')
contador_de_a = 0
contador_de_e = 0
contador_de_i = 0
contador_de_o = 0
contador_de_u = 0
for i in palabra:
	if i in'aA':
		contador_de_a+=1
	if i in 'eE':
		contador_de_e+=1
	if i in 'iI':
		contador_de_i+=1
	if i in 'oO':
		contador_de_o+=1
	if i in 'uU':
		contador_de_u+=1
print(f'las veces que tiene a es: {contador_de_a},\nlas veces que tiene e es: {contador_de_e},\nlas veces que tiene i es: {contador_de_i},\nlas veces que tiene o es: {contador_de_o}\nlas veces que tiene u es: {contador_de_u}')
"""

# Ejercicio 6
"""
precios = [50,75,46,22,80,65,8]
minimo = min(precios)
maximo = max(precios)
print(f'El numero minimo es: {minimo}')
print(f'El numero maximo es: {maximo}')
"""

# Ejercicio 7
"""
diccionario = {'Euro':'€', 'Dollar':'$', 'Yen':'¥'}
pedir = input('Ingrese la divisia: ')
pedir_capi = pedir.capitalize()
if pedir_capi in diccionario:
	print(diccionario[pedir_capi])
else:
	print('La divisia no esta')
"""

# Ejercicio 8
"""
diccionario = {}
persona = ['nombre','edad','direccion','telefono']
nombre = ''
edad = 0
direccion = ''
telefono = 0
while True:
	pedir = input('Quieres agregar una persona nueva\nPresiona 1 para agregar\nO presiona 0 para salir.\n')
	if pedir == '1':
		for i in persona:
			persona_nueva = input(f'Ingresa tu {i}: ')
			diccionario[i] = persona_nueva
			continue
		break
	elif pedir == '0':
		break

for x in persona:
	if x == 'nombre':
		nombre = x
	if x == 'edad':
		edad = x
	if x == 'direccion':
		direccion = x
	if x == 'telefono':
		telefono = x
name = diccionario[nombre]
age = diccionario[edad]
direction = diccionario[direccion]
phone = diccionario[telefono]
print('Tu nombre es {}, tienes {} años, vives en {} y tu numero de telefono es {}'.format(name,age,direction,phone))
"""

# Ejercicio 9
"""
frutas = {'Platano':1.35,'Manzana':0.80,'Pera':0.85,'Naranja':0.70}

nombre_de_fruta = ''
precio = 0
fruta = input('Digite una fruta: ')
for clave,item in frutas.items():
	# print(clave + ' ' + item)
	# fruta = input('Digite una fruta: ')
	if clave in fruta:
		kilos = int(input('Digite los kilos de frutas que desea: '))
		nombre_de_fruta = clave
		# oo = item
		total = item * kilos
		precio = total
		break
	else:
		#print('La fruta que ha ingresado no se ha encontrado.')
		continue
print(precio)
print(f'El precio seria de {precio} de fruta {nombre_de_fruta}')
"""

# Ejercicio 10
"""
diccionario_de_fechas = {}
fechas = ['dd','mm','aaaa']
dato = []

for i in fechas:
	date = input('Ingrese el {}: '.format(i))
	diccionario_de_fechas[i] = date
	dato.append(date)

print(diccionario_de_fechas)
"""

# Ejercicio 11
"""
materias = {'Matemáticas': 6, 'Física': 4, 'Química': 5} 

for clave, item in materias.items():
	print('El curso de {} requiere {} créditos.'.format(clave,item))
"""

# Ejercicio 12
"""
while True:
	persona = {}
	datos_a_pedir = ['nombre','edad','genero','teléfono','correo electronico']
	pedir = input('Presione 1 para agregar una persona\nPresiona 0 para salir\n')
	if pedir == '1':
		for dato in datos_a_pedir:
			new_person = input('Ingrese el {}: '.format(dato))
			persona[dato] = new_person
			print('')
	elif pedir == '0':
		break
	else:
		print('Opcion no validad')
		continue
	print(persona)
"""

# Ejercicio 13
"""
compras = {}
compra_validad = True

while compra_validad == True:
	pedir = int(input('Presiona 1 para agregar un nuevo producto\nPresiona 0 para salir\n'))
	while pedir == 1:
		nombre = input('Ingresa el nombre del articulo: ')
		precio = float(input('Ingrese el precio del articulo: '))
		if nombre != '0' and precio != 0.0:
			compras[nombre] = precio
			print('')
		if precio == 0.0:
			compra_validad = False
			break
	if pedir == 0:
		break

print(compras)
"""

# Ejercicio 14
"""
traduccion = {}
verdadero = True

while verdadero == True:
	pedir = input('Presione 1 para agregar\nPresione 2 para consultar\nPresione 0 para salir\n')
	while pedir == '1':
		spain = input('Ingrese la palabra en español: ')
		if spain != '0':
			ingles = input('Ingrese la traduccion: ')
			traduccion[spain] = ingles
			print('')
		elif spain == '0':
			break
	if pedir == '2':
		buscar = input('Ingrese una palabra en español: ')
		for clave,item in traduccion.items():
			if buscar in clave:
				print(traduccion[buscar])
				verdadero = False
				break
			else:
				verdadero = False
				break
	elif pedir == '0':
		break
"""

# Ejercicio 15
"""
facturas = {}

verdadero = True
contador_de_factura_pagas = 0
contador_de_factura_pendientes = 0

while verdadero == True:
	pedir = input('1. Para agregar una nueva factura\n2. Para pagar una factura existente\n0. Para salir\n')
	if pedir == '1':
		print('')
		numero_de_factura = int(input('Ingrese el numero de factura: '))
		cantidad_de_la_factura = float(input('Ingrese el coste de la factura: '))
		facturas[numero_de_factura] = cantidad_de_la_factura
		contador_de_factura_pendientes += 1
		print('\nLa cantidad de facturas cobradas son {} y la cantidad pendiente de cobro es {}\n'.format(contador_de_factura_pagas,contador_de_factura_pendientes))
		continue
	elif pedir == '2':
		print('')
		numero_de_factura = int(input('Ingrese el numero de la factura que desee pagar: '))
		facturas.pop(numero_de_factura)
		contador_de_factura_pagas += 1
		contador_de_factura_pendientes -= 1
		print('\nLa cantidad de facturas cobradas son {} y la cantidad pendiente de cobro es {}\n'.format(contador_de_factura_pagas,contador_de_factura_pendientes))
		continue
	elif pedir == '0':
		verdadero = False
"""

# Ejercicio 16
"""
clients = {}
campo_dpi = []
campos_cliente = ['DPI','nombres','dirección','teléfono','correo','preferente']
id_client = 1
while True:
	datos_del_cliente = {}
	opcion = int(input('Menu de opciones\n(1) Añadir cliente\n(2) Eliminar cliente\n(3) Mostrar cliente\n(4) Listar todos los clientes\n(5) Listar clientes preferentes\n(6) Salir\n'))
	if opcion == 1:
		suma_id = id_client
		print()
		for i in campos_cliente:
			pedir_datos_del_cliente = input('Ingrese el {} del cliente: '.format(i))
			if i == 'DPI':
				dato_cliente_int = int(pedir_datos_del_cliente)
				datos_del_cliente[i] = dato_cliente_int
			elif i == 'nombres':
				datos_del_cliente[i] = pedir_datos_del_cliente
			elif i == 'dirección':
				datos_del_cliente[i] = pedir_datos_del_cliente
			elif i == 'teléfono':
				dato_cliente_int = int(pedir_datos_del_cliente)
				datos_del_cliente[i] = dato_cliente_int
			elif i == 'correo':
				datos_del_cliente[i] = pedir_datos_del_cliente
			elif i == 'preferente':
				if pedir_datos_del_cliente == 's' or pedir_datos_del_cliente == 'S':
					dato_booleano_s = True
					datos_del_cliente[i] = dato_booleano_s
				elif pedir_datos_del_cliente == 'n' or pedir_datos_del_cliente == 'N':
					dato_booleano_n = False
					datos_del_cliente[i] = dato_booleano_n
		clients[suma_id] = datos_del_cliente
		id_client += 1 
		print()
	elif opcion == 2:
		print()
		dpi_de_cliente = int(input('Ingrese el numero del cliente que desea eliminar: '))
		clientes.pop(dpi_de_cliente)
		print('Cliente Eliminado')
		print()
	elif opcion == 3:
		print()
		numero_de_identificacion_cliente = int(input('Ingrese el numero del cliente: '))
		for clave,item in clients.items():
			if numero_de_identificacion_cliente == clave:
				print('{} {}.'.format(clave,item))
				print()
	elif opcion == 4:
		print()
		print(clients)
		print()
	elif opcion == 5:
		print()
		for claves,item in clients.items():
			if item['preferente']:
				print(item['DPI'],item['nombres'])
				print()
	elif opcion == 6:
		break
"""

# Ejercicio 17
"""
def file(n):
	nombre = 'tabla-' + str(n) + '.txt'
	try:
		fichero = open(nombre,'r')
		data = fichero.read()
		print(data)
	except FileNotFoundError:
		print(f'El fichero {nombre} no existe.')

numero = int(input('Ingrese un numero entre 1 a 10: '))
file(numero)
"""